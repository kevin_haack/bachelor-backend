package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.function.Function;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Kevin Haack
 */
public class Workspace implements Serializable {

    static final long serialVersionUID = 1L;

    private File file;

    private final Set<Function> leafes = new HashSet<>();
    private final Set<Function> functions = new HashSet<>();

    public void addFunctions(Function... functions) {
        for (Function f : functions) {
            addFunction(f);
        }
    }

    public void removeFunctions(Function... functions) {
        for (Function f : functions) {
            removeFunction(f);
        }
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public static Workspace fromFile(File file) {
        InputStream buffer = null;
        Workspace workspace = null;

        try {
            buffer = new BufferedInputStream(new FileInputStream(file));
            ObjectInput input = new ObjectInputStream(buffer);
            workspace = (Workspace) input.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                if (null != buffer) {
                    buffer.close();
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        return workspace;
    }

    public static void toFile(Workspace workspace, File file) {
        OutputStream os = null;

        try {
            os = new FileOutputStream(file);
            OutputStream buffer = new BufferedOutputStream(os);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(workspace);

            output.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                if (null != os) {
                    os.close();
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public void addFunction(Function function) {
        this.leafes.add(function);
        this.functions.add(function);
    }

    private boolean isLeaf(Function function) {
        boolean isLeaf = true;

        for (Function f : this.functions) {
            if (f.isParameter(function)) {
                isLeaf = false;
            }
        }

        return isLeaf;
    }

    public void removeFunction(Function function) {
        this.functions.remove(function);

        // add possible parameters as leaf
        for (int i = 0; i < function.getParameterCount(); i++) {
            if (function.getParameter(i) instanceof Function) {
                Function f = (Function) function.getParameter(i);
                if (isLeaf(f)) {
                    this.leafes.add(f);
                }
            }
        }

        // remove function as parameter
        for (Function f : this.functions) {
            if (f.isParameter(function)) {
                f.removeParameter(function);
            }
        }

        this.leafes.remove(function);
    }

    public void removeInput(Function function, int position) {
        Function parameter = (Function) function.getParameter(position);
        function.removeParameter(position);

        boolean isLeaf = true;
        for (Function f : this.functions) {
            if (f.isParameter(parameter)) {
                isLeaf = false;
            }
        }

        if (isLeaf) {
            this.leafes.add(parameter);
        }
    }

    public void setInput(Function target, Function parameter, int position) {
        if (!CycleChecker.isCycle(target, parameter)) {
            this.leafes.remove(parameter);

            target.setParameter(parameter, position);
        } else {
            throw new RuntimeException("cycle");
        }
    }

    public Set<Function> getLeafes() {
        return this.leafes;
    }
}
