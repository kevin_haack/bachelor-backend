package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.Evaluable;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Ein Objekt dieser Klasse repräsentiert den Namensraum in einem Programm.
 *
 * @author Kevin Haack
 */
public class NameScope {

    /**
     * Der Präfix, der zur Bennenung einer Variable verwendet wird.
     */
    private static final String PREFIX_VARIABLE = "v";
    /**
     * Der Präfix, der zur Bennung eines Skript Alias verwendet wird.
     */
    private static final String PREFIX_SCRIPT = "s";
    /**
     * Die nächste freie ID, die zur Bennenung einer Variablen verwendet wird.
     */
    private int nextVariableID = 0;
    /**
     * Die nächste freie ID, die zur Bennenung eines Skript Alias verwendet
     * wird.
     */
    private int nextScriptID = 0;
    /**
     * Das Mapping, welche Variable welchen Namen zugeteilt bekommen hat.
     */
    private final Map<Evaluable, String> variableMapping = new HashMap<>();
    /**
     * Das Mapping, welches Skript welchen Alias zugeteilt bekommen hat.
     */
    private final Map<File, String> scriptMapping = new HashMap<>();

    /**
     * Registriert das übergebene 'Evaluable' im Namesraum.
     *
     * @param evaluable
     */
    public void registerVariable(Evaluable evaluable) {
        this.variableMapping.put(evaluable, PREFIX_VARIABLE + this.nextVariableID);

        this.nextVariableID++;
    }

    /**
     * Registriert die übergebene Datei als Skript im Namesraum.
     *
     * @param file
     */
    public void registerScript(File file) {
        this.scriptMapping.put(file, PREFIX_SCRIPT + this.nextScriptID);

        this.nextScriptID++;
    }

    /**
     * Zeigt ob das übergebene 'Evaluable' im Namesraum registriert ist.
     *
     * @param evaluable Das zu prüfende 'Evaluable'.
     * @return Liefert 'true', wenn das 'Evaluable' registriert ist.
     */
    public boolean isVariableRegistered(Evaluable evaluable) {
        return this.variableMapping.containsKey(evaluable);
    }

    /**
     * Zeigt ob die Übergebene Datei im Namensraum registriert ist.
     *
     * @param file Die zu prüfende Datei.
     * @return Liefert 'true', wenn die Datei registriert ist.
     */
    public boolean isScriptRegistered(File file) {
        return this.scriptMapping.containsKey(file);
    }

    /**
     * Liefert den gemappten Variablennamen für das übergebene 'Evaluatable'.
     *
     * @param evaluable 'Evaluatable' nach dessen Name gefragt ist.
     * @return Liefert Namen des 'Evaluatable' oder 'null', falls 'Evaluatable'
     * nicht registriert ist.
     */
    public String getVariableName(Evaluable evaluable) {
        return this.variableMapping.get(evaluable);
    }

    /**
     * Liefert den gemappten Skript Alias für die übergebe Datei.
     *
     * @param file Datei nach deren Alias gefragt ist.
     * @return Liefert Alias der übergebenen Datei oder 'null', falls die Datei
     * nicht registriert ist.
     */
    public String getScriptAlias(File file) {
        return this.scriptMapping.get(file);
    }

    /**
     * Setzt den Namensraum auf den Ursprungszustand zurück.
     */
    public void reset() {
        this.nextVariableID = 0;
        this.nextScriptID = 0;
        this.variableMapping.clear();
    }
}
