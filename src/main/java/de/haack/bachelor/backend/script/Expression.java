package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.NameScope;
import java.io.File;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Ein Object dieser Klasse repärsentiert einen Ausdruck.
 *
 * @author Kevin Haack
 */
public class Expression implements Evaluable, Serializable {

    static final long serialVersionUID = 1L;
    /**
     * Der Code dieses Ausdrucks
     */
    private final String code;
    /**
     * Alle benötigten Imports.
     */
    private final Set<Import> imports;

    public Expression(String code) {
        this.code = code;
        this.imports = new HashSet<>();
    }

    public Expression(String code, Set<Import> imports) {
        this.code = code;
        this.imports = imports;
    }

    /**
     * Liefert den code dieses Ausdrucks.
     *
     * @return Der Code.
     */
    public String getCode() {
        return code;
    }

    @Override
    public LineOfCode getDefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        return new LineOfCode(this.code);
    }

    @Override
    public List<LineOfCode> getPredefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        return new LinkedList<>();
    }

    @Override
    public List<File> getRequiredFiles() {
        return new LinkedList<>();
    }
    
    public static String getIndentationSpaces(int indentationLevel) {
        StringBuilder builder = new StringBuilder("");
        
        for(int i = 0; i < indentationLevel; i++) {
            builder.append("    ");
        }
        
        return builder.toString();
    }

    @Override
    public List<LineOfCode> getPostdefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        return new LinkedList<>();
    }
}
