package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.script.function.FunctionHeader;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Diese Factory dient zum Erzeugen 'Script' Objecten aus einem 'File' Object.
 *
 * @author Kevin Haack
 */
public class ScriptFactory {

    protected ScriptFactory() {

    }

    /**
     * Erzeugt aus der übergebenen 'LineOfCode' ein Import. Vorraussetzung dafür
     * ist, dass die 'LineOfCode' wirklich einen Import darstellt.
     *
     * @param lineOfCode
     * @return
     */
    protected Import createImport(LineOfCode lineOfCode) {
        if (lineOfCode.isImport()) {
            String code = lineOfCode.getCode().replaceFirst("import ", "");
            String name = code.trim();

            return new Import(name);
        } else if (lineOfCode.isFromImport()) {
            String[] parts = lineOfCode.getCode().split(" ");

            return new Import(parts[1], parts[3]);
        } else {
            throw new IllegalArgumentException("The passed lineOfCode is no import.");
        }
    }

    /**
     * Erzeugt aus der übergebenen 'LineOfCode' ein 'FunctionHeader'.
     * Vorraussetzung dafür ist, dass die 'LineOfCode' wirklich einen
     * 'FunctionHeader' darstellt.
     *
     * @param file Das 'File' Objekt aus dem die 'LineOfCode' stammt.
     * @param requiredImports Alle bentigten Imports aus der Skriptdatei.
     * @param lineOfCode Die 'LineOfCode' die den 'FunctionHeader'
     * repräsentiert.
     * @return Den erzeugten 'FunctionHeader'.
     */
    protected FunctionHeader createFunctionHeader(File file, Set<Import> requiredImports, LineOfCode lineOfCode) {
        if (!lineOfCode.isFunctionHeader()) {
            throw new IllegalArgumentException("The passed lineOfCode is no functionHeader.");
        }

        String code = lineOfCode.getCode().replaceFirst("def ", "");
        String[] parts = code.split("\\(");

        // name
        String name = parts[0].trim();

        // parameter
        parts[1] = parts[1].replace(")", "").replace(":", "").trim();
        String[] parameters = parts[1].split(",");

        int parameterCount;
        if (parameters.length == 0
                || parameters[0].isEmpty()) {
            parameterCount = 0;
        } else {
            parameterCount = parameters.length;
        }

        return new FunctionHeader(file, name, parameterCount, requiredImports);
    }

    /**
     * Erzeugt aus den übergebenen 'LinesOfCode' ein 'Script' Object.
     *
     * @param file Das 'File' Objekt aus dem die 'LinesOfCode' stamment.
     * @param linesOfCode Alle 'LinesOfCode' aus der Skriptdatei.
     * @return Das erzeugte 'Script' Objekt.
     */
    public Script create(File file, List<LineOfCode> linesOfCode) {
        Set<Import> imports = new HashSet<>();
        List<FunctionHeader> functionHeaders = new LinkedList<>();

        for (LineOfCode lineOfCode : linesOfCode) {
            if (lineOfCode.isImport()) {
                imports.add(createImport(lineOfCode));
            } else if (lineOfCode.isFunctionHeader()) {
                functionHeaders.add(createFunctionHeader(file, imports, lineOfCode));
            }
        }

        return new Script(file, functionHeaders);
    }
}
