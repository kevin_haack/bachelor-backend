package de.haack.bachelor.backend.script;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Ein Objekt dieser Klasse dient zum einlesen von 'File' Objekten und daraus
 * 'Scripts' zu erzeugen.
 *
 * @author Kevin Haack
 */
public class ScriptLoader {

    /**
     * Liefert alle 'LinesOfCode' aus dem übergebenen 'InputStream'.
     *
     * @param inputStream 'InputStream' aus dem gelesen werden soll.
     * @return Alle aus dem 'InputStream' gelesenen 'LinesOfCode'.
     */
    protected List<LineOfCode> getLinesOfCode(InputStream inputStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        List<LineOfCode> linesOfCode = new LinkedList<>();

        try {
            String line = reader.readLine();

            while (null != line) {
                linesOfCode.add(new LineOfCode(line));

                line = reader.readLine();
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        return linesOfCode;
    }

    /**
     * Liest das übergebene 'File' Objekt ein und liefert das daraus erzeugte
     * 'Script' Objekt.
     *
     * @param file 'File' aus der gelesen werden soll.
     * @return Das erzeugte Skript.
     */
    public Script load(File file) {
        List<LineOfCode> linesOfCode;

        try {
            linesOfCode = getLinesOfCode(new FileInputStream(file));
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        }

        ScriptFactory factory = new ScriptFactory();

        return factory.create(file, linesOfCode);
    }
}
