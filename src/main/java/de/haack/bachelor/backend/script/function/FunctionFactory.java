package de.haack.bachelor.backend.script.function;

/**
 *
 * @author Kevin Haack
 */
public class FunctionFactory {
    
    private static int nextId = 0;
    
    private FunctionFactory() {
        
    }
    
   public static void reset() {
       FunctionFactory.nextId = 0;
   }
    
    public static Function create(FunctionHeader functionHeader) {
        Function f = new Function(functionHeader, FunctionFactory.nextId, 0, 0);
        FunctionFactory.nextId++;
        
        return f;
    }

    public static Function create(FunctionHeader functionHeader, double x, double y) {
        Function f = new Function(functionHeader, FunctionFactory.nextId, x, y);
        FunctionFactory.nextId++;
        
        return f;
    }
}
