package de.haack.bachelor.backend.script;

import java.io.Serializable;
import java.util.Objects;

/**
 * Ein Object von dieser Klasse repräsentiert eine Zeile Code.
 *
 * @author Kevin Haack
 */
public class LineOfCode implements Serializable {

    static final long serialVersionUID = 1L;

    /**
     * Die Zeiele Code.
     */
    private String code;

    public LineOfCode() {
        this.code = "";
    }

    public LineOfCode(String code) {
        this.code = code;
    }

    /**
     * Liefert die Zeile Code.
     *
     * @return Die Zeile Code.
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Setzt die Zeile Code.
     *
     * @param code Die Zeile Code.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Zeigt an ob die Zeile Code ein Import repräsentiert.
     *
     * @return 'true', falls die Zeile ein Import ist.
     */
    public boolean isImport() {
        return this.code.matches("(import ([A-Za-z0-9]+.)+)");
    }
    
    /**
     * Zeigt an ob die Zeile Code ein Import mit der "From" Anweisung ist.
     */
    public boolean isFromImport() {
        return this.code.matches("(from ([A-Za-z0-9]+.)+ import ([A-Za-z0-9]+.)+)");
    }

    /**
     * Zeigt an ob die Zeile Code ein Funktionskopf ist.
     *
     * @return 'true', falls die Zeile ein Funktionskopf ist.
     */
    public boolean isFunctionHeader() {
        return this.code.startsWith("def");
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LineOfCode other = (LineOfCode) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }
}
