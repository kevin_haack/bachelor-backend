package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.NameScope;
import de.haack.bachelor.backend.script.function.Function;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Ein Objekt dieser Klasse repräsentiert eine Variable. Eine Variable besteht
 * aus ihrem Namen und aus einem 'Evaluable'.
 *
 * @author Kevin Haack
 */
public class Variable implements Serializable {

    static final long serialVersionUID = 1L;

    /**
     * Der Wert der Variable.
     */
    private final Evaluable evaluable;

    public Variable(Evaluable value) {
        this.evaluable = value;
    }

    /**
     * Liefert den in dem Namensraum gemappten Namen für diese Variable.
     *
     * @param nameScope Der Namensraum des Programms.
     * @return Der Name der Variable oder 'null', falls der 'Evaluable' noch
     * nicht registriert ist.
     */
    public String getName(NameScope nameScope) {
        return nameScope.getVariableName(this.evaluable);
    }

    /**
     * Der Wert der Variable.
     *
     * @return Der Wert der Variable.
     */
    public Evaluable getEvaluable() {
        return evaluable;
    }

    /**
     * Liefert den definierenden Code für diese Variable.
     *
     * Zum Beispiel: 'v0 = 5' 'v0 = null'
     *
     * @param indentationLevel Anzahl der Einrückungen.
     * @param nameScope Der Namensraum des Programms.
     *
     * @return Der definierenden Code für diese Variable oder null, falls kein
     * 'Evaluable' für diese Varibale gesetzt ist.
     */
    public List<LineOfCode> getDefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        if (null != this.evaluable) {
            List<LineOfCode> linesOfCode = new LinkedList<>();

            linesOfCode.addAll(this.evaluable.getPredefiningCode(indentationLevel, nameScope, includeGateway));

            String indentationSpaces = Expression.getIndentationSpaces(indentationLevel);

            if (includeGateway && this.evaluable instanceof Function) {
                indentationSpaces += Expression.getIndentationSpaces(1);
            }

            linesOfCode.add(new LineOfCode(indentationSpaces + nameScope.getVariableName(this.evaluable) + " = " + this.evaluable.getDefiningCode(0, nameScope, false).getCode()));
            linesOfCode.addAll(this.evaluable.getPostdefiningCode(indentationLevel, nameScope, includeGateway));

            return linesOfCode;
        } else {
            return null;
        }
    }

}
