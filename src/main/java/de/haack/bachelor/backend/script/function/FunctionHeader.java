package de.haack.bachelor.backend.script.function;

import de.haack.bachelor.backend.script.Import;
import java.io.File;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * Ein Objekt dieser Klasse repräsentiert einen Funktionskopf.
 *
 * @author Kevin Haack
 */
public class FunctionHeader implements Serializable {

    static final long serialVersionUID = 1L;

    /**
     * Die Skriptdatei aus dem der Funktionskopf stammt.
     */
    private final File file;
    /**
     * Der Name des Funktionskopfs.
     */
    private final String name;
    /**
     * Die Anzahl der Parameter in dem Funktionskopf.
     */
    private final int parameterCount;
    /**
     * Alle benötigten Imports für diesen Funktionskopf.
     */
    private final Set<Import> requiredImports;

    public FunctionHeader(File file, String name, int parameterCount, Set<Import> requiredImports) {
        this.file = file;
        this.name = name;
        this.parameterCount = parameterCount;
        this.requiredImports = requiredImports;
    }

    /**
     * Liefert alle nötigen Imports.
     *
     * @return Alle nötigen Imports oder ein leeres 'Set', falls keine Imports
     * benötigt werden.
     */
    public Set<Import> getRequiredImports() {
        return requiredImports;
    }

    /**
     * Liefert das 'File' Objekt, aus dem der Funktionskopf stammt.
     *
     * @return 'File' Objekt, aus dem der Funktionskopf stammt.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Liefert den Namen der Funktion.
     *
     * @return Namen der Funktion.
     */
    public String getName() {
        return name;
    }

    /**
     * Liefert die Anzahl der Parameter dieser Funktion.
     *
     * @return Anzahl der Parameter dieser Funktion.
     */
    public int getParameterCount() {
        return parameterCount;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.file);
        hash = 43 * hash + Objects.hashCode(this.name);
        hash = 43 * hash + this.parameterCount;
        hash = 43 * hash + Objects.hashCode(this.requiredImports);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FunctionHeader other = (FunctionHeader) obj;
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.parameterCount != other.parameterCount) {
            return false;
        }
        if (!Objects.equals(this.requiredImports, other.requiredImports)) {
            return false;
        }
        return true;
    }
}
