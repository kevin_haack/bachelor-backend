package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.script.function.FunctionHeader;
import java.io.File;
import java.io.Serializable;
import java.util.LinkedList;

import java.util.List;
import java.util.Objects;

/**
 * Ein Object dieser Klasse repräsentiert eine Skriptdatei.
 *
 * @author Kevin Haack
 */
public class Script implements Serializable {

    static final long serialVersionUID = 1L;
    /**
     * Die Skriptdatei.
     */
    private final File file;
    /**
     * Alle in der Skriptdatei enhaltenen 'FunctionHeaders'.
     */
    private final List<FunctionHeader> functionHeaders;

    public Script(File file, List<FunctionHeader> functionHeaders) {
        this.file = file;
        this.functionHeaders = functionHeaders;
    }

    /**
     * Liefert alle in der Skriptdatei enthaltenen 'FunctionHeaders'.
     *
     * @return Liefert alle in der Skriptdatei enthaltenen 'FunctionHeaders'
     * oder eine leere Liste, falls keine 'FunctionHeaders' angegeben wurden.
     */
    public List<FunctionHeader> getFunctionHeaders() {
        if (null != this.functionHeaders) {
            return this.functionHeaders;
        } else {
            return new LinkedList<>();
        }
    }

    /**
     * Liefert das 'File' Objekt zu dieser Skriptdatei.
     *
     * @return Das 'File' Objekt zu dieser Skriptdatei.
     */
    public File getFile() {
        return file;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.file.getAbsolutePath());
        hash = 97 * hash + Objects.hashCode(this.functionHeaders);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Script other = (Script) obj;
        if (!Objects.equals(this.file.getAbsolutePath(), other.file.getAbsolutePath())) {
            return false;
        }
        if (!Objects.equals(this.functionHeaders, other.functionHeaders)) {
            return false;
        }
        return true;
    }
}
