package de.haack.bachelor.backend.script;

import java.io.Serializable;
import java.util.Objects;

/**
 * Ein Object dieser Klasse repräsentiert einen Import.
 *
 * @author Kevin Haack
 */
public class Import implements Serializable {

    static final long serialVersionUID = 1L;

    /**
     * Der Name des Items, dass importiert wird.
     */
    private final String item;
    /*
     * Der Name des Modules, aus dem das Importierte Item stammt.
     */
    private final String module;

    public Import(String module, String item) {
        this.module = module;
        this.item = item;
    }
    
    public Import(String item) {
        this.module = null;
        this.item = item;
    }

    /**
     * Liefert den definierenden Code für den Import.
     *
     * @return Liefert den definierenden Code für den Import oder 'null', falls
     * der 'ModuleName' 'null' ist.
     */
    public LineOfCode getDefiningCode() {
        if (null != this.item) {
            if(null == this.module) {
                return new LineOfCode("import " + this.item);
            } else {
                return new LineOfCode("from " + this.module + " import " + this.item);
            }
        } else {
            throw new IllegalStateException("Import item not set.");
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.item);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Import other = (Import) obj;
        if (!Objects.equals(this.item, other.item)) {
            return false;
        }
        return true;
    }

}
