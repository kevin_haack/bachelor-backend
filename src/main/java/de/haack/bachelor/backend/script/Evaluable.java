package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.NameScope;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Alle Klassen die dieses Interface implementieren sind auswertebare Elemente
 * in einem Programm.
 *
 * @author Kevin Haack
 */
public interface Evaluable {

    /**
     * Liefert den definierenden Code.
     *
     * @param indentationLevel Anzahl der Einrückungen
     * @param nameScope Der Namesraum des Programms.
     * @return Liefert den definierenden Code.
     */
    LineOfCode getDefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway);

    /**
     * Liefert den vorher zu definierenden Code.
     *
     * @param indentationLevel Anzahl der Einrückungen
     * @param nameScope Der Namesraum des Programms.
     * @return Liefert den vorher zu definierenden Code oder eine leere 'List',
     * falls kein Code vorher definiert wird.
     */
    List<LineOfCode> getPredefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway);

    List<LineOfCode> getPostdefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway);

    /**
     * Liefert alle verwendeten 'FunctionHeaders' von diesem Element und von
     * seinen abhängigen.
     *
     * @return Liefert alle verwendeten 'FunctionHeaders' von diesem Element und
     * von seinen abhängigen oder eine leeres 'Set', falls keine
     * 'FunctionHeaders' verwendet werden.
     */
    List<File> getRequiredFiles();
}
