package de.haack.bachelor.backend.script.function;

import de.haack.bachelor.backend.NameScope;
import de.haack.bachelor.backend.Point;
import de.haack.bachelor.backend.script.Evaluable;
import de.haack.bachelor.backend.script.Expression;
import de.haack.bachelor.backend.script.LineOfCode;
import de.haack.bachelor.backend.script.Variable;
import java.io.File;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Ein Objekt dieser Klasse repräsentiert eine Funktion.
 *
 * @author Kevin Haack
 */
public class Function implements Evaluable, Serializable {

    static final long serialVersionUID = 1L;

    /**
     * Die Position der Funktion auf dem Workspace.
     */
    private final Point position = new Point();
    /**
     * Der 'FunctionHeader' der für diese Funktion definiert ist.
     */
    private final FunctionHeader functionHeader;
    /**
     * Die Parameter die für diese Funktion gesetzt wurden.
     */
    private final Evaluable[] parameters;
    /**
     * Das Mapping der Parameter auf ihre im Namensraum definierten
     * Variablennamen.
     */
    private final Map<Evaluable, Variable> tempParameters = new HashMap<>();

    private final int id;

    /**
     * Liefert die Position der Funktion auf dem Workspace.
     *
     * @return
     */
    public Point getPosition() {
        return position;
    }

    protected Function(FunctionHeader functionHeader, int id, double x, double y) {
        this.functionHeader = functionHeader;
        this.id = id;
        this.parameters = new Evaluable[functionHeader.getParameterCount()];
        this.position.x = x;
        this.position.y = y;
    }

    public int getId() {
        return id;
    }
    
    public boolean isRoot() {
        boolean isRoot = true;
        
        for(Evaluable e: this.parameters) {
            if(null != e) {
                isRoot = false;
            }
        }
        
        return isRoot;
    }

    /**
     * Liefert die Anzahl der Parameter, die für diese Funktion benötigt werden.
     *
     * @return Anzahl der Parameter.
     */
    public int getParameterCount() {
        return this.parameters.length;
    }

    /**
     * Liefert den Parameter, der an der übergebenen Position ist.
     *
     * @param position Position des gewünschten Parameters.
     * @return Parameter an der übergebenen Position.
     */
    public Evaluable getParameter(int position) {
        return this.parameters[position];
    }

    /**
     * Entfernt den Parameter an der übergebenen Positin.
     *
     * @param position Position des zu entfernen Parameters.
     */
    public void removeParameter(int position) {
        this.parameters[position] = null;
    }

    /**
     * Zeigt an, ob irgendein Parameter dieser Funltion gesetzt ist.
     *
     * @return 'true' falls irgend ein Parameter dieser Funktion gesetzt ist.
     */
    public boolean isAnyParameterSet() {
        boolean any = false;

        for (Evaluable e : parameters) {
            if (null != e) {
                any = true;
            }
        }

        return any;
    }

    /**
     * Setzt den übergebenen 'Evaluable' als Parameter an der übergebenen
     * Position.
     *
     * @param evaluable 'Evaluable' als Parameter.
     * @param position Positon.
     */
    public void setParameter(Evaluable evaluable, int position) {
        this.parameters[position] = evaluable;
        this.tempParameters.put(evaluable, null);
    }

    @Override
    public List<LineOfCode> getPredefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        List<LineOfCode> linesOfCode = new LinkedList<>();

        for (Evaluable e : getUsedEvaluables()) {
            // first call
            if (!nameScope.isVariableRegistered(e)) {
                nameScope.registerVariable(e);
                Variable variable = new Variable(e);

                linesOfCode.addAll(variable.getDefiningCode(indentationLevel, nameScope, includeGateway));
            }

            // create deepCopy
            Expression deepCopy = createDeepCopyExpression(e, nameScope);
            nameScope.registerVariable(deepCopy);

            // create new variable
            Variable variable = new Variable(deepCopy);

            this.tempParameters.put(e, variable);
            linesOfCode.addAll(variable.getDefiningCode(indentationLevel, nameScope, includeGateway));
        }

        if (includeGateway) {
            String indentationSpaces = Expression.getIndentationSpaces(indentationLevel);

            linesOfCode.add(new LineOfCode(indentationSpaces + "try:"));
        }

        return linesOfCode;
    }

    /**
     * Zeigt an, ob der übergebene 'Evaluable' ein Parameter der Funktion ist.
     *
     * @param evaluable Der potenzielle 'Evaluable'.
     * @return 'true' falls der 'Evaluable' ein Parameter der Funktion ist.
     */
    public boolean isParameter(Evaluable evaluable) {
        return Arrays.asList(this.parameters).contains(evaluable);
    }

    /**
     * Entfernt den übergebenen 'Evaluable' von allen Positionen der Funktion.
     *
     * @param evaluable Der zu entfernene 'Evaluable'.
     */
    public void removeParameter(Evaluable evaluable) {
        int i = 0;

        while (i < this.parameters.length) {
            if (this.parameters[i].equals(evaluable)) {
                this.parameters[i] = null;
            }
            i++;
        }
    }

    @Override
    public LineOfCode getDefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        StringBuilder builder = new StringBuilder();

        // indentation spaces
        if (includeGateway) {
            builder.append(Expression.getIndentationSpaces(1));
        }

        // script alieas
        builder.append(Expression.getIndentationSpaces(indentationLevel));
        
        if(null !=nameScope.getScriptAlias(this.functionHeader.getFile())) {
            builder.append(nameScope.getScriptAlias(this.functionHeader.getFile()));
        } else {
            builder.append("None");
        }
        
        builder.append(".");

        // function name
        builder.append(this.functionHeader.getName());

        // parameters
        builder.append(" ( ");

        for (int i = 0; i < this.parameters.length; i++) {
            if (null != this.parameters[i]) {
                if(null != nameScope.getVariableName(this.tempParameters.get(this.parameters[i]).getEvaluable())) {
                    builder.append(nameScope.getVariableName(this.tempParameters.get(this.parameters[i]).getEvaluable()));
                } else {
                    builder.append("None");
                }
                
            } else {
                builder.append(new Expression("None").getDefiningCode(0, nameScope, includeGateway).getCode());
            }

            if (i < this.parameters.length - 1) {
                builder.append(", ");
            }
        }

        builder.append(" )");

        return new LineOfCode(builder.toString());
    }

    /**
     * Liefert alle in der Funktion und deren Parameter verwendeten 'Evaluable'.
     *
     * @return Alle verwendeten 'Evaluable'.
     */
    private List<Evaluable> getUsedEvaluables() {
        List<Evaluable> evals = new LinkedList<>();

        for (Evaluable e : this.parameters) {
            if (null != e) {
                if (!evals.contains(e)) {
                    evals.add(e);
                }
            }
        }

        return evals;
    }

    /**
     * Erzeugt 'Expression', die eine 'deep copy' des Ergebnisses dieses
     * übergebenen 'Evaluable' erzeugt.
     *
     * @param evaluable 'Evaluable' für den die 'deep copy' erzeugt werden soll.
     * @param nameScope Namensraum dieser Funktion.
     * @return
     */
    private Expression createDeepCopyExpression(Evaluable evaluable, NameScope nameScope) {
        String variableName = nameScope.getVariableName(evaluable);

        return new Expression("copy.deepcopy ( " + variableName + " )");
    }

    /**
     * Liefert 'FunctionsHeader' dieser Funktion.
     *
     * @return 'FunctionsHeader'
     */
    public FunctionHeader getFunctionHeader() {
        return functionHeader;
    }

    /**
     * Liefert alle verwendeten Funktionen die in diesem Ast der Komposition
     * verwendet werden. (exklusive dieser Instanz)
     *
     * @return Alle verwendeten Funktionen die in diesem Ast der Komposition
     * verwendet.
     */
    public Set<Function> getAllUsedFunctions() {
        Set<Function> functions = new HashSet<>();

        for (Evaluable e : this.parameters) {
            if (null != e
                    && e instanceof Function) {
                functions.addAll(((Function) e).getAllUsedFunctions());
            }
        }

        return functions;
    }

    @Override
    public List<File> getRequiredFiles() {
        List<File> files = new LinkedList<>();

        if (!files.contains(this.functionHeader.getFile())) {
            files.add(this.functionHeader.getFile());
        }

        for (Evaluable e : this.parameters) {
            if (null != e) {
                for (File f : e.getRequiredFiles()) {
                    if (!files.contains(f)) {
                        files.add(f);
                    }
                }
            }
        }

        return files;
    }

    @Override
    public List<LineOfCode> getPostdefiningCode(int indentationLevel, NameScope nameScope, boolean includeGateway) {
        List<LineOfCode> linesOfCodes = new LinkedList<>();
        if (includeGateway) {
            String indentationSpaces = Expression.getIndentationSpaces(indentationLevel);

            linesOfCodes.add(new LineOfCode(indentationSpaces + "except Exception as e:"));
            linesOfCodes.add(new LineOfCode(indentationSpaces + "    communicator.onException( " + this.id + " , str(e) )"));
            linesOfCodes.add(new LineOfCode(indentationSpaces + "    quit()"));
        }
        return linesOfCodes;
    }
}
