package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.function.Function;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Diese Klasse enthält methoden um Kreise in verketteten Funktionen zu
 * entdecken.
 *
 * @author Kevin Haack
 */
public class CycleChecker {

    private CycleChecker() {
    }

    /**
     * Die Methode prüft, ob das Hinzufügen der Funktion 'other' einen Kreis
     * schließen würde.
     *
     * @param current Eine der Funktionen, die bereits in der Komositions
     * enthalten sind.
     * @param other Die Funktion, die hinzugefügt werden soll.
     * @return Liefert 'true', wenn das Hinzufügen dieser Funktion einen Kreis
     * schließen würde.
     */
    public static boolean isCycle(Function current, Function other) {

        Set<Function> visited = new HashSet<>();
        Stack<Function> stack = new Stack<>();

        stack.add(other);
        while (!stack.isEmpty()) {
            Function p = stack.pop();

            if (!visited.contains(p)) {
                visited.add(p);

                // add all parameter to stack
                for (int n = 0; n < p.getParameterCount(); n++) {
                    if (null != p.getParameter(n)
                            && p.getParameter(n) instanceof Function) {
                        Function pp = (Function) p.getParameter(n);

                        if (!visited.contains(pp)) {
                            stack.add(pp);
                        }
                    }
                }
            }

        }

        return visited.contains(current);
    }
}
