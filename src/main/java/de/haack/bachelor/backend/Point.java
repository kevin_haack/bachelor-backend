package de.haack.bachelor.backend;

import java.io.Serializable;

/**
 * Ein Objekt dieser Klasse repräsentiert einen Punkt in einem zwei
 * Dimensionalen Raum.
 *
 * @author Kevin Haack
 */
public class Point implements Serializable {

    static final long serialVersionUID = 1L;

    /**
     * Die X Koordinate.
     */
    public double x;
    /**
     * Die Y Koordinate.
     */
    public double y;
}
