package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.Import;
import de.haack.bachelor.backend.script.LineOfCode;
import java.util.List;
import java.util.Set;

/**
 * Ein Objekt dieser Klasse repräsentiert ein Programm.
 * @author Kevin Haack
 */
public class Program {
    /**
     * Alle für dieses Programm nötigen Imports.
     */
    private final Set<Import> imports;

    private final List<LineOfCode> preCode;
    /**
     * Alle in diesem Programm enthaltenen 'linesOfCode'.
     */
    private final List<LineOfCode> linesOfCode;

    public Program(Set<Import> imports, List<LineOfCode> preCode, List<LineOfCode> linesOfCode) {
        this.imports = imports;
        this.linesOfCode = linesOfCode;
        this.preCode = preCode;
    }

    public List<LineOfCode> preCode() {
        return preCode;
    }
    
    /**
     * Liefert alle Imports.
     * @return Imports.
     */
    public Set<Import> getImports() {
        return imports;
    }

    /**
     * Liefert alle 'linesOfCode'.
     * @return 'linesOfCode'.
     */
    public List<LineOfCode> getLinesOfCode() {
        return linesOfCode;
    }
}
