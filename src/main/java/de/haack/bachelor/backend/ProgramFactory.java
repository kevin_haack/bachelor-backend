package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.Evaluable;
import de.haack.bachelor.backend.script.Expression;
import de.haack.bachelor.backend.script.Import;
import de.haack.bachelor.backend.script.LineOfCode;
import de.haack.bachelor.backend.script.Variable;
import de.haack.bachelor.backend.script.function.Function;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * Dieses Factory dient zum Erstellen vom Programmen.
 *
 * @author Kevin Haack
 */
public class ProgramFactory {

    /**
     * Liefert ein neu erzeugtes 'Program' generiert aus den übergebenen 'Blatt
     * Evaluable'. Bindet nicht das Javagateway in das Programm ein.
     *
     * @param leafs Die Blätter der zu generierenden Funktionskomposition.
     * @return Liefert erzeugtes 'Program'.
     */
    public Program create(Evaluable... leafs) {
        return create(false, leafs);
    }

    public Program createLoop(boolean includeJavaGateway, Evaluable... leafs) {
        if (leafs.length == 0) {
            throw new RuntimeException("no leafs passed");
        }

        NameScope nameScope = new NameScope();
        List<LineOfCode> linesOfCode = new Stack<>();

        // imports
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("copy"));
        imports.add(new Import("imp"));

        // gateway
        List<LineOfCode> preCode = new LinkedList<>();
        if (includeJavaGateway) {
            imports.add(new Import("py4j.java_gateway", "JavaGateway"));

            preCode.add(new LineOfCode("gateway = JavaGateway()"));
            preCode.add(new LineOfCode("communicator = gateway.entry_point.getCommunicator()"));
            preCode.add(new LineOfCode("communicator.programStart()"));
            preCode.add(new LineOfCode("try:"));
        }

        // scripts
        List<File> files = new LinkedList<>();
        for (Evaluable e : leafs) {
            for (File f : e.getRequiredFiles()) {
                if (!files.contains(f)) {
                    files.add(f);
                }
            }
        }

        for (File f : files) {
            nameScope.registerScript(f);
            preCode.addAll(getScriptAliasLines(f, nameScope, includeJavaGateway));
        }

        // root
        Set<Function> roots = new HashSet<>();
        for (Evaluable e : leafs) {
            roots.addAll(getRoots((Function) e));
        }

        Function root;
        if (roots.size() == 1) {
            root = (Function) roots.toArray()[0];
        } else if (roots.size() == 0) {
            throw new RuntimeException("no roots passed");
        } else {
            throw new RuntimeException("too many roots (" + roots.size() + ")");
        }

        nameScope.registerVariable(root);
        Variable rootVariable = new Variable(root);

        linesOfCode.addAll(rootVariable.getDefiningCode((includeJavaGateway ? 1 : 0), nameScope, includeJavaGateway));

        // start while
        String indentationSpaces = Expression.getIndentationSpaces(includeJavaGateway ? 1 : 0);
        linesOfCode.add(new LineOfCode(indentationSpaces + "while " + nameScope.getVariableName(root) + " is not None:"));

        // branche
        for (Evaluable e : leafs) {
            if (!e.equals(root)) {
                int indentationLevel = 1 + (includeJavaGateway ? 1 : 0);
                Function function = (Function) e;

                linesOfCode.addAll(function.getPredefiningCode(indentationLevel, nameScope, includeJavaGateway));
                linesOfCode.add(function.getDefiningCode(indentationLevel, nameScope, includeJavaGateway));
                linesOfCode.addAll(function.getPostdefiningCode(indentationLevel, nameScope, includeJavaGateway));
            }
        }

        // end while
        linesOfCode.addAll(rootVariable.getDefiningCode(1 + (includeJavaGateway ? 1 : 0), nameScope, includeJavaGateway));

        // gateway
        if (includeJavaGateway) {
            linesOfCode.add(new LineOfCode("except Exception as e:"));
            linesOfCode.add(new LineOfCode("    communicator.onException( None , str(e) )"));
            linesOfCode.add(new LineOfCode("communicator.programEnd()"));
        }

        return new Program(imports, preCode, linesOfCode);
    }

    private Set<Function> getRoots(Function function) {
        Set<Function> roots = new HashSet<>();

        if (function.isRoot()) {
            roots.add(function);
        } else {
            for (int i = 0; i < function.getParameterCount(); i++) {
                if (null != function.getParameter(i)) {
                    roots.addAll(getRoots((Function) function.getParameter(i)));
                }
            }
        }

        return roots;
    }

    /**
     * Liefert ein neu erzeugtes 'Program' generiert aus den übergebenen 'Blatt
     * Evaluable'.
     *
     * @param includeJavaGateway Ob das Javagateway in das Programm eingebunden
     * werden soll.
     * @param leafs Die Blätter der zu generierenden Funktionskomposition.
     * @return Liefert erzeugtes 'Program'.
     */
    public Program create(boolean includeJavaGateway, Evaluable... leafs) {
        if (leafs.length == 0) {
            throw new RuntimeException("no leafs passed");
        }

        NameScope nameScope = new NameScope();
        List<LineOfCode> linesOfCode = new Stack<>();

        // imports
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("copy"));
        imports.add(new Import("imp"));

        // gateway
        List<LineOfCode> preCode = new LinkedList<>();

        if (includeJavaGateway) {
            imports.add(new Import("py4j.java_gateway", "JavaGateway"));

            preCode.add(new LineOfCode("gateway = JavaGateway()"));
            preCode.add(new LineOfCode("communicator = gateway.entry_point.getCommunicator()"));
            preCode.add(new LineOfCode("communicator.programStart()"));
            preCode.add(new LineOfCode("try:"));
        }

        // scripts
        List<File> files = new LinkedList<>();
        for (Evaluable e : leafs) {
            for (File f : e.getRequiredFiles()) {
                if (!files.contains(f)) {
                    files.add(f);
                }
            }
        }

        for (File f : files) {
            nameScope.registerScript(f);
            preCode.addAll(getScriptAliasLines(f, nameScope, includeJavaGateway));
        }

        // branches
        for (Evaluable e : leafs) {
            int indentationLevel = (includeJavaGateway ? 1 : 0);
                Function function = (Function) e;

                linesOfCode.addAll(function.getPredefiningCode(indentationLevel, nameScope, includeJavaGateway));
                linesOfCode.add(function.getDefiningCode(indentationLevel, nameScope, includeJavaGateway));
                linesOfCode.addAll(function.getPostdefiningCode(indentationLevel, nameScope, includeJavaGateway));
        }

        // gateway
        if (includeJavaGateway) {
            linesOfCode.add(new LineOfCode("except Exception as e:"));
            linesOfCode.add(new LineOfCode("    communicator.onException( None , str(e) )"));
            linesOfCode.add(new LineOfCode("communicator.programEnd()"));
        }

        return new Program(imports, preCode, linesOfCode);
    }

    /**
     * Liefert die 'LineOfCode', die das Skript Alias definiert.
     *
     * @param nameScope Der Namensraum des Programms.
     * @return 'LineOfCode' die den Skript Alias definiert.
     */
    private List<LineOfCode> getScriptAliasLines(File file, NameScope nameScope, boolean includeJavaGateway) {
        List<LineOfCode> lines = new LinkedList<>();
        String moduleName = file.getName().replaceFirst("[.][^.]+$", "");
        StringBuilder builder = new StringBuilder();

        builder.append(nameScope.getScriptAlias(file));
        builder.append(" = imp.load_source('");
        builder.append(moduleName);
        builder.append("', '");
        builder.append(file.getAbsolutePath());
        builder.append("')");

        if (includeJavaGateway) {
            lines.add(new LineOfCode("    try:"));
            lines.add(new LineOfCode("        " + builder.toString()));
            lines.add(new LineOfCode("    except Exception as e:"));
            lines.add(new LineOfCode("        communicator.onException( None , str(e) )"));
            lines.add(new LineOfCode("        quit()"));
        } else {
            lines.add(new LineOfCode(builder.toString()));
        }

        return lines;
    }
}
