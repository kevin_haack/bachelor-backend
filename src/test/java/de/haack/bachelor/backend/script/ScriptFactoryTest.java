package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.script.function.FunctionHeader;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kevin Haack
 */
public class ScriptFactoryTest {
    
    public ScriptFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testCreateImport() {
        System.out.println("createImport");
        LineOfCode lineOfCode = new LineOfCode("import test");
        ScriptFactory instance = new ScriptFactory();
        
        Import expResult = new Import("test");
        Import result = instance.createImport(lineOfCode);
        
        assertEquals(expResult.getDefiningCode(), result.getDefiningCode());
    }
    
    @Test
    public void testCreateImport1() {
        System.out.println("createImport (from import)");
        LineOfCode lineOfCode = new LineOfCode("from testModule import testItem");
        ScriptFactory instance = new ScriptFactory();
        
        Import expResult = new Import("testModule", "testItem");
        Import result = instance.createImport(lineOfCode);
        
        assertEquals(expResult.getDefiningCode(), result.getDefiningCode());
    }

    @Test
    public void testCreateFunctionHeader1() {
        System.out.println("createFunctionHeader1");
        LineOfCode lineOfCode = new LineOfCode("def main()");
        
        ScriptFactory instance = new ScriptFactory();
        FunctionHeader expResult = new FunctionHeader(new File("C:/Path/to/file.py"), "main", 0, new HashSet<Import>());
        FunctionHeader result = instance.createFunctionHeader(new File("C:/Path/to/file.py"), new HashSet<Import>(), lineOfCode);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateFunctionHeader2() {
        System.out.println("createFunctionHeader2");
        LineOfCode lineOfCode = new LineOfCode("def main(v1)");
        
        ScriptFactory instance = new ScriptFactory();
        FunctionHeader expResult = new FunctionHeader(new File("C:/Path/to/file.py"), "main", 1, new HashSet<Import>());
        FunctionHeader result = instance.createFunctionHeader(new File("C:/Path/to/file.py"), new HashSet<Import>(), lineOfCode);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateFunctionHeader3() {
        System.out.println("createFunctionHeader3");
        LineOfCode lineOfCode = new LineOfCode("def main(v1, v2)");
        
        ScriptFactory instance = new ScriptFactory();
        FunctionHeader expResult = new FunctionHeader(new File("C:/Path/to/file.py"), "main", 2, new HashSet<Import>());
        FunctionHeader result = instance.createFunctionHeader(new File("C:/Path/to/file.py"), new HashSet<Import>(), lineOfCode);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateFunctionHeader4() {
        System.out.println("createFunctionHeader4");
        LineOfCode lineOfCode = new LineOfCode("def main(v1, v2, v3)");
        
        ScriptFactory instance = new ScriptFactory();
        FunctionHeader expResult = new FunctionHeader(new File("C:/Path/to/file.py"), "main", 3, new HashSet<Import>());
        FunctionHeader result = instance.createFunctionHeader(new File("C:/Path/to/file.py"), new HashSet<Import>(), lineOfCode);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of create method, of class ScriptFactory.
     */
    @Test
    public void testCreate1() {
        /*
         * GIVEN
         */
        System.out.println("create1");
        File file = new File("C:/Path/to/file.py");
        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("import test1"));
        linesOfCode.add(new LineOfCode("import test2"));
        linesOfCode.add(new LineOfCode("import test3"));
        
        linesOfCode.add(new LineOfCode("def main1():"));
        linesOfCode.add(new LineOfCode("    some code"));
        linesOfCode.add(new LineOfCode("    more code"));
        linesOfCode.add(new LineOfCode(""));
        
        linesOfCode.add(new LineOfCode("def main2(v1):"));
        linesOfCode.add(new LineOfCode("    some code"));
        linesOfCode.add(new LineOfCode("    more code"));
        
        linesOfCode.add(new LineOfCode("def main3(v1, v2):"));
        linesOfCode.add(new LineOfCode("    some code"));
        linesOfCode.add(new LineOfCode("    more code"));
        
        linesOfCode.add(new LineOfCode("def main4(v1, v2, v3):"));
        linesOfCode.add(new LineOfCode("    some code"));
        linesOfCode.add(new LineOfCode("    more code"));
        
        /*
         * EXPECTED
         */
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("test1"));
        imports.add(new Import("test2"));
        imports.add(new Import("test3"));
        
        List<FunctionHeader> functionHeaders = new LinkedList<>();
        functionHeaders.add(new FunctionHeader(new File("C:/Path/to/file.py"), "main1", 0, imports));
        functionHeaders.add(new FunctionHeader(new File("C:/Path/to/file.py"), "main2", 1, imports));
        functionHeaders.add(new FunctionHeader(new File("C:/Path/to/file.py"), "main3", 2, imports));
        functionHeaders.add(new FunctionHeader(new File("C:/Path/to/file.py"), "main4", 3, imports));
        
        Script expResult = new Script(new File("C:/Path/to/file.py"), functionHeaders);
        
        
        /*
         * CHECK
         */
        ScriptFactory instance = new ScriptFactory();
        Script result = instance.create(file, linesOfCode);
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreate2() {
        /*
         * GIVEN
         */
        System.out.println("create2");
        File file = new File("C:/Path/to/file.py");
        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("import math"));
        linesOfCode.add(new LineOfCode("### EXAMPLE PYTHON MODULE"));
        linesOfCode.add(new LineOfCode("# Define some variables:"));
        linesOfCode.add(new LineOfCode("numberone = 1"));
        linesOfCode.add(new LineOfCode("ageofqueen = 78"));
        linesOfCode.add(new LineOfCode(""));
        linesOfCode.add(new LineOfCode("# define some functions"));
        linesOfCode.add(new LineOfCode("def printhello():"));
        linesOfCode.add(new LineOfCode("    print \"hello\""));
        linesOfCode.add(new LineOfCode("    "));
        linesOfCode.add(new LineOfCode("def timesfour(input):"));
        linesOfCode.add(new LineOfCode("    print input * 4"));
        
        /*
         * EXPECTED
         */
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));
        
        List<FunctionHeader> functionHeaders = new LinkedList<>();
        functionHeaders.add(new FunctionHeader(new File("C:/Path/to/file.py"), "printhello", 0, imports));
        functionHeaders.add(new FunctionHeader(new File("C:/Path/to/file.py"), "timesfour", 1, imports));
        
        Script expResult = new Script(new File("C:/Path/to/file.py"), functionHeaders);
        
        
        /*
         * CHECK
         */
        ScriptFactory instance = new ScriptFactory();
        Script result = instance.create(file, linesOfCode);
        
        assertEquals(expResult, result);
    }
    
}
