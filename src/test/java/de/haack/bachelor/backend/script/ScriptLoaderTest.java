package de.haack.bachelor.backend.script;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kevin Haack
 */
public class ScriptLoaderTest {

    public ScriptLoaderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getLinesOfCode method, of class ScriptLoader.
     */
    @Test
    public void testGetLinesOfCode() {
        System.out.println("getLinesOfCode");
        StringBuilder builder = new StringBuilder();

        builder.append("import math\n");
        builder.append("### EXAMPLE PYTHON MODULE\n");
        builder.append("# Define some variables:\n");
        builder.append("numberone = 1\n");
        builder.append("ageofqueen = 78\n");
        builder.append("\n");
        builder.append("# define some functions\n");
        builder.append("def printhello():\n");
        builder.append("    print \"hello\"\n");
        builder.append("    \n");
        builder.append("def timesfour(input):\n");
        builder.append("    print input * 4");

        InputStream inputStream = new ByteArrayInputStream(builder.toString().getBytes(StandardCharsets.UTF_8));;
        ScriptLoader instance = new ScriptLoader();

        List<LineOfCode> expResult = new LinkedList<>();
        expResult.add(new LineOfCode("import math"));
        expResult.add(new LineOfCode("### EXAMPLE PYTHON MODULE"));
        expResult.add(new LineOfCode("# Define some variables:"));
        expResult.add(new LineOfCode("numberone = 1"));
        expResult.add(new LineOfCode("ageofqueen = 78"));
        expResult.add(new LineOfCode(""));
        expResult.add(new LineOfCode("# define some functions"));
        expResult.add(new LineOfCode("def printhello():"));
        expResult.add(new LineOfCode("    print \"hello\""));
        expResult.add(new LineOfCode("    "));
        expResult.add(new LineOfCode("def timesfour(input):"));
        expResult.add(new LineOfCode("    print input * 4"));
        
        List<LineOfCode> result = instance.getLinesOfCode(inputStream);
        assertEquals(expResult, result);
    }

}
