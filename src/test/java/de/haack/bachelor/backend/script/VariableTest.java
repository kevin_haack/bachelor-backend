package de.haack.bachelor.backend.script;

import de.haack.bachelor.backend.NameScope;
import de.haack.bachelor.backend.script.function.Function;
import de.haack.bachelor.backend.script.function.FunctionFactory;
import de.haack.bachelor.backend.script.function.FunctionHeader;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kevin Haack
 */
public class VariableTest {
    
    public VariableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetDefiningCode1() {
        System.out.println("getDefiningCode1");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        Expression ex0 = new Expression("42");
        nameScope.registerVariable(ex0);
        
        Variable instance = new Variable(ex0);
        
        // EXPECTED
        List<LineOfCode> expResult = new LinkedList<>();
        expResult.add(new LineOfCode("v0 = 42"));
        
        // TEST
        List<LineOfCode> result = instance.getDefiningCode(0, nameScope, false);
        
        if(expResult.size() != result.size()) {
            fail("EXPECTED lenght " + expResult.size() + ", but was " + result.size());
        }
        
        for(int i = 0; i < result.size(); i++) {
            assertEquals(expResult.get(i).getCode(), result.get(i).getCode());
        }
    }
    
    @Test
    public void testGetDefiningCode2() {
        System.out.println("getDefiningCode2");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Function function = FunctionFactory.create(new FunctionHeader(f1, "main", 0, new HashSet<Import>()));
        
        Variable instance = new Variable(function);
        
        nameScope.registerVariable(function);
        
        // EXPECTED
        List<LineOfCode> expResult = new LinkedList<>();
        expResult.add(new LineOfCode("v0 = s0.main (  )"));
        
        // TEST
        List<LineOfCode> result = instance.getDefiningCode(0, nameScope, false);
        
        if(expResult.size() != result.size()) {
            fail("EXPECTED lenght " + expResult.size() + ", but was " + result.size());
        }
        
        for(int i = 0; i < result.size(); i++) {
            assertEquals(expResult.get(i).getCode(), result.get(i).getCode());
        }
    }
    
    @Test
    public void testGetDefiningCode3() {
        System.out.println("getDefiningCode3");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("1337");
        Expression ex1 = new Expression("1338");
        
        Function function = FunctionFactory.create(new FunctionHeader(f1, "main", 2, new HashSet<Import>()));
        function.setParameter(ex0, 0);
        function.setParameter(ex1, 1);
        
        nameScope.registerVariable(function);
        
        Variable instance = new Variable(function);
        
        // EXPECTED
        List<LineOfCode> expResult = new LinkedList<>();
        expResult.add(new LineOfCode("v1 = 1337"));
        expResult.add(new LineOfCode("v2 = copy.deepcopy ( v1 )"));
        expResult.add(new LineOfCode("v3 = 1338"));
        expResult.add(new LineOfCode("v4 = copy.deepcopy ( v3 )"));
        expResult.add(new LineOfCode("v0 = s0.main ( v2, v4 )"));
        
        // TEST
        List<LineOfCode> result = instance.getDefiningCode(0, nameScope, false);
        
        if(expResult.size() != result.size()) {
            fail("EXPECTED lenght " + expResult.size() + ", but was " + result.size());
        }
        
        for(int i = 0; i < result.size(); i++) {
            assertEquals(expResult.get(i).getCode(), result.get(i).getCode());
        }
    }
    
    @Test
    public void testGetDefiningCode4() {
        System.out.println("getDefiningCode4 (indentationLevel)");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        Expression ex0 = new Expression("42");
        nameScope.registerVariable(ex0);
        
        Variable instance = new Variable(ex0);
        
        // EXPECTED
        List<LineOfCode> expResult = new LinkedList<>();
        expResult.add(new LineOfCode("    v0 = 42"));
        
        // TEST
        List<LineOfCode> result = instance.getDefiningCode(1, nameScope, false);
        
        if(expResult.size() != result.size()) {
            fail("EXPECTED lenght " + expResult.size() + ", but was " + result.size());
        }
        
        for(int i = 0; i < result.size(); i++) {
            assertEquals(expResult.get(i).getCode(), result.get(i).getCode());
        }
    }
    
    @Test
    public void testGetDefiningCode5() {
        System.out.println("getDefiningCode5 (indentationLevel)");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("1337");
        Expression ex1 = new Expression("1338");
        
        Function function = FunctionFactory.create(new FunctionHeader(f1, "main", 2, new HashSet<Import>()));
        function.setParameter(ex0, 0);
        function.setParameter(ex1, 1);
        
        nameScope.registerVariable(function);
        
        Variable instance = new Variable(function);
        
        // EXPECTED
        List<LineOfCode> expResult = new LinkedList<>();
        expResult.add(new LineOfCode("    v1 = 1337"));
        expResult.add(new LineOfCode("    v2 = copy.deepcopy ( v1 )"));
        expResult.add(new LineOfCode("    v3 = 1338"));
        expResult.add(new LineOfCode("    v4 = copy.deepcopy ( v3 )"));
        expResult.add(new LineOfCode("    v0 = s0.main ( v2, v4 )"));
        
        // TEST
        List<LineOfCode> result = instance.getDefiningCode(1, nameScope, false);
        
        if(expResult.size() != result.size()) {
            fail("EXPECTED lenght " + expResult.size() + ", but was " + result.size());
        }
        
        for(int i = 0; i < result.size(); i++) {
            assertEquals(expResult.get(i).getCode(), result.get(i).getCode());
        }
    }
}
