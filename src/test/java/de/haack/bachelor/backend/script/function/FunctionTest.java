package de.haack.bachelor.backend.script.function;

import de.haack.bachelor.backend.NameScope;
import de.haack.bachelor.backend.script.Expression;
import de.haack.bachelor.backend.script.Import;
import de.haack.bachelor.backend.script.LineOfCode;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kevin Haack
 */
public class FunctionTest {
    
    public FunctionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetDefiningCode1() {
        System.out.println("getDefiningCode1");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        Function instance = FunctionFactory.create(new FunctionHeader(new File("C:/Path/to/file.py"), "main", 0, new HashSet<Import>()));
        
        // EXPECTED (test without script in nameScrope)
        LineOfCode expResult = new LineOfCode("None.main (  )");
        
        // TEST
        LineOfCode result = instance.getDefiningCode(0, nameScope, false);
        
        assertEquals(expResult.getCode(), result.getCode());
    }
    
  @Test
    public void testGetPredefiningCode1() {
        System.out.println("getPredefiningCode1");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("42");
        
        Function instance = FunctionFactory.create(new FunctionHeader(f1, "main", 1, new HashSet<Import>()));
        instance.setParameter(ex0, 0);
        
        // EXPECTED
        List<LineOfCode> expPredefiningCode = new LinkedList<>();
        expPredefiningCode.add(new LineOfCode("v0 = 42"));
        
        expPredefiningCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        
        LineOfCode expLineOfCode = new LineOfCode("s0.main ( v1 )");
        
        // TEST getPredefiningCode
        List<LineOfCode> resultPredefiningCode = instance.getPredefiningCode(0, nameScope, false);

        if (expPredefiningCode.size() != resultPredefiningCode.size()) {
            fail("EXPECTED lenght " + expPredefiningCode.size() + ", but was " + resultPredefiningCode.size());
        }

        for (int i = 0; i < resultPredefiningCode.size(); i++) {
            assertEquals(expPredefiningCode.get(i).getCode(), resultPredefiningCode.get(i).getCode());
        }
        
        // TEST
        LineOfCode resultLineOfCode = instance.getDefiningCode(0, nameScope, false);
        
        assertEquals(expLineOfCode.getCode(), resultLineOfCode.getCode());
    }
    
    @Test
    public void testGetPredefiningCode2() {
        System.out.println("testGetPredefiningCode2");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("42");
        Expression ex1 = new Expression("43");
        
        Function instance = FunctionFactory.create(new FunctionHeader(f1, "main", 2, new HashSet<Import>()));
        instance.setParameter(ex0, 0);
        instance.setParameter(ex1, 1);
        
        // EXPECTED
        List<LineOfCode> expPredefiningCode = new LinkedList<>();
        expPredefiningCode.add(new LineOfCode("v0 = 42"));
        expPredefiningCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        expPredefiningCode.add(new LineOfCode("v2 = 43"));
        expPredefiningCode.add(new LineOfCode("v3 = copy.deepcopy ( v2 )"));
        
        LineOfCode expLineOfCode = new LineOfCode("s0.main ( v1, v3 )");

        // TEST getPredefiningCode
        List<LineOfCode> resultPredefiningCode = instance.getPredefiningCode(0, nameScope, false);

        if (expPredefiningCode.size() != resultPredefiningCode.size()) {
            fail("EXPECTED lenght " + expPredefiningCode.size() + ", but was " + resultPredefiningCode.size());
        }

        for (int i = 0; i < resultPredefiningCode.size(); i++) {
            assertEquals(expPredefiningCode.get(i).getCode(), resultPredefiningCode.get(i).getCode());
        }
        
        // TEST
        LineOfCode resultLineOfCode = instance.getDefiningCode(0, nameScope, false);
        
        assertEquals(expLineOfCode.getCode(), resultLineOfCode.getCode());
    }
    
    @Test
    public void testGetPredefiningCode3() {
        System.out.println("testGetPredefiningCode3");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("42");
        Expression ex1 = new Expression("43");
        Expression ex2 = new Expression("44");
        
        Function instance = FunctionFactory.create(new FunctionHeader(f1, "main", 3, new HashSet<Import>()));
        instance.setParameter(ex0, 0);
        instance.setParameter(ex1, 1);
        instance.setParameter(ex2, 2);
        
        // EXPECTED
        List<LineOfCode> expPredefiningCode = new LinkedList<>();
        expPredefiningCode.add(new LineOfCode("v0 = 42"));
        expPredefiningCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        expPredefiningCode.add(new LineOfCode("v2 = 43"));
        expPredefiningCode.add(new LineOfCode("v3 = copy.deepcopy ( v2 )"));
        expPredefiningCode.add(new LineOfCode("v4 = 44"));
        expPredefiningCode.add(new LineOfCode("v5 = copy.deepcopy ( v4 )"));
        
        LineOfCode expLineOfCode = new LineOfCode("s0.main ( v1, v3, v5 )");
        
        // TEST getPredefiningCode
        List<LineOfCode> resultPredefiningCode = instance.getPredefiningCode(0, nameScope, false);

        if (expPredefiningCode.size() != resultPredefiningCode.size()) {
            fail("EXPECTED lenght " + expPredefiningCode.size() + ", but was " + resultPredefiningCode.size());
        }

        for (int i = 0; i < resultPredefiningCode.size(); i++) {
            assertEquals(expPredefiningCode.get(i).getCode(), resultPredefiningCode.get(i).getCode());
        }
        
        // TEST
        LineOfCode resultLineOfCode = instance.getDefiningCode(0, nameScope, false);
        
        assertEquals(expLineOfCode.getCode(), resultLineOfCode.getCode());
    }
    
    @Test
    public void testGetDefiningCode5() {
        System.out.println("getDefiningCode5");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("42");
        Expression ex1 = new Expression("43");
        
        Function instance = FunctionFactory.create(new FunctionHeader(f1, "main", 3, new HashSet<Import>()));
        instance.setParameter(ex0, 0);
        instance.setParameter(ex1, 1);
        
        // EXPECTED
        List<LineOfCode> expPredefiningCode = new LinkedList<>();
        expPredefiningCode.add(new LineOfCode("v0 = 42"));
        expPredefiningCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        expPredefiningCode.add(new LineOfCode("v2 = 43"));
        expPredefiningCode.add(new LineOfCode("v3 = copy.deepcopy ( v2 )"));
        
        LineOfCode expLineOfCode = new LineOfCode("s0.main ( v1, v3, None )");

        // TEST getPredefiningCode
        List<LineOfCode> resultPredefiningCode = null;
                
        try {
            resultPredefiningCode = instance.getPredefiningCode(0, nameScope, false);
        } catch (NullPointerException ex) {
            fail("Should not throw NullPointerException, if one parameter is missing");
        }

        if (expPredefiningCode.size() != resultPredefiningCode.size()) {
            fail("EXPECTED lenght " + expPredefiningCode.size() + ", but was " + resultPredefiningCode.size());
        }

        for (int i = 0; i < resultPredefiningCode.size(); i++) {
            assertEquals(expPredefiningCode.get(i).getCode(), resultPredefiningCode.get(i).getCode());
        }
        
        // TEST
        LineOfCode resultLineOfCode = instance.getDefiningCode(0, nameScope, false);
        
        assertEquals(expLineOfCode.getCode(), resultLineOfCode.getCode());
    }
    
    @Test
    public void testGetDefiningCode6() {
        System.out.println("getDefiningCode6 (indentationLevel)");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("42");
        Expression ex1 = new Expression("43");
        
        Function instance = FunctionFactory.create(new FunctionHeader(f1, "main", 3, new HashSet<Import>()));
        instance.setParameter(ex0, 0);
        instance.setParameter(ex1, 1);
        
        // EXPECTED
        List<LineOfCode> expPredefiningCode = new LinkedList<>();
        expPredefiningCode.add(new LineOfCode("    v0 = 42"));
        expPredefiningCode.add(new LineOfCode("    v1 = copy.deepcopy ( v0 )"));
        expPredefiningCode.add(new LineOfCode("    v2 = 43"));
        expPredefiningCode.add(new LineOfCode("    v3 = copy.deepcopy ( v2 )"));
        
        LineOfCode expLineOfCode = new LineOfCode("    s0.main ( v1, v3, None )");

        // TEST getPredefiningCode
        List<LineOfCode> resultPredefiningCode = null;
                
        try {
            resultPredefiningCode = instance.getPredefiningCode(1, nameScope, false);
        } catch (NullPointerException ex) {
            fail("Should not throw NullPointerException, if one parameter is missing");
        }

        if (expPredefiningCode.size() != resultPredefiningCode.size()) {
            fail("EXPECTED lenght " + expPredefiningCode.size() + ", but was " + resultPredefiningCode.size());
        }

        for (int i = 0; i < resultPredefiningCode.size(); i++) {
            assertEquals(expPredefiningCode.get(i).getCode(), resultPredefiningCode.get(i).getCode());
        }
        
        // TEST
        LineOfCode resultLineOfCode = instance.getDefiningCode(1, nameScope, false);
        
        assertEquals(expLineOfCode.getCode(), resultLineOfCode.getCode());
        
        // TEST getPostdefiningCode
        List<LineOfCode> resultPostdefiningCode = null;
                
        try {
            resultPostdefiningCode = instance.getPostdefiningCode(0, nameScope, false);
        } catch (NullPointerException ex) {
            fail("Should not throw NullPointerException, if one parameter is missing");
        }

        if (0 != resultPostdefiningCode.size()) {
            fail("EXPECTED lenght 0, but was " + resultPostdefiningCode.size());
        }
    }
    
   @Test
    public void testGetDefiningCode7() {
        System.out.println("getDefiningCode7 (with gateway)");
        
        // GIVEN
        NameScope nameScope = new NameScope();
        
        File f1 = new File("C:/Path/to/file.py");
        nameScope.registerScript(f1);
        
        Expression ex0 = new Expression("42");
        
        FunctionFactory.reset();
        Function instance = FunctionFactory.create(new FunctionHeader(f1, "main", 1, new HashSet<Import>()));
        instance.setParameter(ex0, 0);
        
        // EXPECTED
        List<LineOfCode> expPredefiningCode = new LinkedList<>();
        expPredefiningCode.add(new LineOfCode("v0 = 42"));
        expPredefiningCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        expPredefiningCode.add(new LineOfCode("try:"));
        
        LineOfCode expLineOfCode = new LineOfCode("    s0.main ( v1 )");
        
        List<LineOfCode> expPostdefiningCode = new LinkedList<>();
        expPostdefiningCode.add(new LineOfCode("except Exception as e:"));
        expPostdefiningCode.add(new LineOfCode("    communicator.onException( 0 , str(e) )"));
        expPostdefiningCode.add(new LineOfCode("    quit()"));

        // TEST getPredefiningCode
        List<LineOfCode> resultPredefiningCode = null;
                
        try {
            resultPredefiningCode = instance.getPredefiningCode(0, nameScope, true);
        } catch (NullPointerException ex) {
            fail("Should not throw NullPointerException, if one parameter is missing");
        }

        if (expPredefiningCode.size() != resultPredefiningCode.size()) {
            fail("EXPECTED lenght " + expPredefiningCode.size() + ", but was " + resultPredefiningCode.size());
        }

        for (int i = 0; i < resultPredefiningCode.size(); i++) {
            assertEquals(expPredefiningCode.get(i).getCode(), resultPredefiningCode.get(i).getCode());
        }
        
        // TEST getDefiningCode
        LineOfCode resultLineOfCode = instance.getDefiningCode(0, nameScope, true);
        
        assertEquals(expLineOfCode.getCode(), resultLineOfCode.getCode());
        
        // TEST getPostdefiningCode
        List<LineOfCode> resultPostdefiningCode = null;
                
        try {
            resultPostdefiningCode = instance.getPostdefiningCode(0, nameScope, true);
        } catch (NullPointerException ex) {
            fail("Should not throw NullPointerException, if one parameter is missing");
        }

        if (expPostdefiningCode.size() != resultPostdefiningCode.size()) {
            fail("EXPECTED lenght " + expPostdefiningCode.size() + ", but was " + resultPostdefiningCode.size());
        }

        for (int i = 0; i < resultPostdefiningCode.size(); i++) {
            assertEquals(expPostdefiningCode.get(i).getCode(), resultPostdefiningCode.get(i).getCode());
        }
    }
}
