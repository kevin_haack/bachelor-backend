package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.Import;
import de.haack.bachelor.backend.script.function.Function;
import de.haack.bachelor.backend.script.function.FunctionFactory;
import de.haack.bachelor.backend.script.function.FunctionHeader;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kevin Haack
 */
public class WorkspaceTest {

    public WorkspaceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getLeaves method, of class Workspace.
     */
    @Test
    public void testGetLeaves1() {
        System.out.println("getLeaves1");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function = FunctionFactory.create(new FunctionHeader(file, "main", 0, imports));

        Workspace instance = new Workspace();
        instance.addFunction(function);

        // EXPECTED
        Set<Function> expResult = new HashSet<>();
        expResult.add(function);

        // TEST
        Set<Function> result = instance.getLeafes();

        assertEquals(expResult, result);
    }

    @Test
    public void testGetLeaves2() {
        System.out.println("getLeaves2");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 0, imports));
        Function function2 = FunctionFactory.create(new FunctionHeader(file, "main2", 0, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1, function2);

        // EXPECTED
        Set<Function> expResult = new HashSet<>();
        expResult.add(function1);
        expResult.add(function2);

        // TEST
        Set<Function> result = instance.getLeafes();

        assertEquals(expResult, result);
    }

    @Test
    public void testGetLeaves3() {
        System.out.println("getLeaves3");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 0, imports));
        Function function2 = FunctionFactory.create(new FunctionHeader(file, "main2", 0, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1, function2);

        // EXPECTED
        Set<Function> expResult = new HashSet<>();
        expResult.add(function2);

        // TEST
        instance.removeFunction(function1);

        Set<Function> result = instance.getLeafes();

        assertEquals(expResult, result);
    }

    @Test
    public void testGetLeaves4() {
        System.out.println("getLeaves4");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 0, imports));
        Function function2 = FunctionFactory.create(new FunctionHeader(file, "main2", 1, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1, function2);

        // EXPECTED
        Set<Function> expResult = new HashSet<>();
        expResult.add(function2);

        // TEST
        instance.setInput(function2, function1, 0);

        Set<Function> result = instance.getLeafes();

        assertEquals(expResult, result);
    }

    @Test
    public void testGetLeaves5() {
        System.out.println("getLeaves5");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 0, imports));
        Function function2 = FunctionFactory.create(new FunctionHeader(file, "main2", 1, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1, function2);
        instance.setInput(function2, function1, 0);

        // EXPECTED
        Set<Function> expResult = new HashSet<>();
        expResult.add(function1);
        expResult.add(function2);

        // TEST
        instance.removeInput(function2, 0);

        Set<Function> result = instance.getLeafes();

        assertEquals(expResult, result);
    }

    @Test
    public void testGetLeaves6() {
        System.out.println("getLeaves6 (cycle)");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 1, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1);

        // TEST
        try {
            instance.setInput(function1, function1, 0);
            fail("should detect cycle");
        } catch (RuntimeException ex) {
            if (ex.getMessage().equals("cycle")) {
                // EXPECTED
                assertTrue(true);
            } else {
                fail(ex.getMessage());
            }
        }
    }
    
    @Test
    public void testGetLeaves7() {
        System.out.println("getLeaves7 (cycle)");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 1, imports));
        Function function2 = FunctionFactory.create(new FunctionHeader(file, "main2", 1, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1);
        instance.addFunctions(function2);

        // TEST
        instance.setInput(function1, function2, 0);
        
        try {
            instance.setInput(function2, function1, 0);
            fail("should detect cycle");
        } catch (RuntimeException ex) {
            if (ex.getMessage().equals("cycle")) {
                // EXPECTED
                assertTrue(true);
            } else {
                fail(ex.getMessage());
            }
        }
    }

    @Test
    public void testGetLeaves8() {
        System.out.println("getLeaves8 (not cycle)");

        // GIVEN
        File file = new File("E:\\test\\path.py");

        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        Function function1 = FunctionFactory.create(new FunctionHeader(file, "main1", 0, imports));
        Function function2 = FunctionFactory.create(new FunctionHeader(file, "main2", 2, imports));

        Workspace instance = new Workspace();
        instance.addFunctions(function1, function2);

        // TEST
        instance.setInput(function2, function1, 0);
        instance.setInput(function2, function1, 1);

        // EXPECTED
        assertTrue(true);
    }

}
