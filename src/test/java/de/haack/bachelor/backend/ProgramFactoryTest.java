package de.haack.bachelor.backend;

import de.haack.bachelor.backend.script.Evaluable;
import de.haack.bachelor.backend.script.Import;
import de.haack.bachelor.backend.script.LineOfCode;
import de.haack.bachelor.backend.script.function.Function;
import de.haack.bachelor.backend.script.function.FunctionFactory;
import de.haack.bachelor.backend.script.function.FunctionHeader;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kevin Haack
 */
public class ProgramFactoryTest {

    public ProgramFactoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of create method, of class ProgramFactory.
     */
    @Test
    public void testCreate1() {
        System.out.println("create1");

        // GIVEN || main
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file.py"), "main", 0, imports);

        FunctionFactory.reset();
        Evaluable leaf = FunctionFactory.create(fh1);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file', 'C:\\Path\\to\\file.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();

        linesOfCode.add(new LineOfCode("s0.main (  )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(leaf);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate2() {
        System.out.println("create2");

        // GIVEN || main2 -> main1
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file1.py"), "main1", 1, imports);
        FunctionHeader fh2 = new FunctionHeader(new File("C:\\Path\\to\\file2.py"), "main2", 0, new HashSet<Import>());

        FunctionFactory.reset();
        Function main1 = FunctionFactory.create(fh1);
        Function main2 = FunctionFactory.create(fh2);

        main1.setParameter(main2, 0);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        
        linesOfCode.add(new LineOfCode("v0 = s1.main2 (  )"));
        linesOfCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("s0.main1 ( v1 )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(main1);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate3() {
        System.out.println("create3");

        // GIVEN || main2 -> main1 -> main0
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:/Path/to/file0.py"), "main0", 1, new HashSet<Import>());
        FunctionHeader fh2 = new FunctionHeader(new File("C:/Path/to/file1.py"), "main1", 1, imports);
        FunctionHeader fh3 = new FunctionHeader(new File("C:/Path/to/file2.py"), "main2", 0, new HashSet<Import>());

        Set<FunctionHeader> functionHeaders = new HashSet<>();
        functionHeaders.add(fh1);
        functionHeaders.add(fh2);
        functionHeaders.add(fh3);

        FunctionFactory.reset();
        Function main0 = FunctionFactory.create(fh1);
        Function main1 = FunctionFactory.create(fh2);
        Function main2 = FunctionFactory.create(fh3);

        main1.setParameter(main2, 0);
        main0.setParameter(main1, 0);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file0', 'C:\\Path\\to\\file0.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));
        expScripts.add(new LineOfCode("s2 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("v1 = s2.main2 (  )"));
        linesOfCode.add(new LineOfCode("v2 = copy.deepcopy ( v1 )"));
        linesOfCode.add(new LineOfCode("v0 = s1.main1 ( v2 )"));
        linesOfCode.add(new LineOfCode("v3 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("s0.main0 ( v3 )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(main0);

        // TEST imports
        if (expResult.getImports().size() != result.getImports().size()) {
            fail("EXPECTED lenght " + expResult.getImports().size() + ", but was " + result.getImports().size());
        }

        assertEquals(expResult.getImports(), result.getImports());

        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }
        
        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate4() {
        System.out.println("create4");

        /* GIVEN
         *       |-> main1
         * main2 |
         *       |-> main0
         */
        Set<Import> imports = new HashSet<>();

        FunctionHeader fh1 = new FunctionHeader(new File("C:/Path/to/file0.py"), "main0", 1, new HashSet<Import>());
        FunctionHeader fh2 = new FunctionHeader(new File("C:/Path/to/file1.py"), "main1", 1, imports);
        FunctionHeader fh3 = new FunctionHeader(new File("C:/Path/to/file2.py"), "main2", 0, new HashSet<Import>());

        FunctionFactory.reset();
        Function main0 = FunctionFactory.create(fh1);
        Function main1 = FunctionFactory.create(fh2);
        Function main2 = FunctionFactory.create(fh3);

        main0.setParameter(main2, 0);
        main1.setParameter(main2, 0);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file0', 'C:\\Path\\to\\file0.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));
        expScripts.add(new LineOfCode("s2 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("v0 = s1.main2 (  )"));
        linesOfCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("s0.main0 ( v1 )"));
        linesOfCode.add(new LineOfCode("v2 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("s2.main1 ( v2 )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(main0, main1);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate5() {
        System.out.println("create5");

        /* GIVEN
         * main0 |
         *       |-> main2
         * main1 |
         */
        FunctionHeader fh1 = new FunctionHeader(new File("C:/Path/to/file0.py"), "main0", 0, new HashSet<Import>());
        FunctionHeader fh2 = new FunctionHeader(new File("C:/Path/to/file1.py"), "main1", 0, new HashSet<Import>());
        FunctionHeader fh3 = new FunctionHeader(new File("C:/Path/to/file2.py"), "main2", 2, new HashSet<Import>());

        FunctionFactory.reset();
        Function main0 = FunctionFactory.create(fh1);
        Function main1 = FunctionFactory.create(fh2);
        Function main2 = FunctionFactory.create(fh3);

        main2.setParameter(main0, 0);
        main2.setParameter(main1, 1);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file0', 'C:\\Path\\to\\file0.py')"));
        expScripts.add(new LineOfCode("s2 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("v0 = s1.main0 (  )"));
        linesOfCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("v2 = s2.main1 (  )"));
        linesOfCode.add(new LineOfCode("v3 = copy.deepcopy ( v2 )"));
        linesOfCode.add(new LineOfCode("s0.main2 ( v1, v3 )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(main2);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate6() {
        System.out.println("create6");

        /* GIVEN
         * main3 -> main0 |
         *                |-> main2
         *          main1 |
         */
        FunctionHeader fh1 = new FunctionHeader(new File("C:/Path/to/file0.py"), "main0", 1, new HashSet<Import>());
        FunctionHeader fh2 = new FunctionHeader(new File("C:/Path/to/file1.py"), "main1", 0, new HashSet<Import>());
        FunctionHeader fh3 = new FunctionHeader(new File("C:/Path/to/file2.py"), "main2", 2, new HashSet<Import>());
        FunctionHeader fh4 = new FunctionHeader(new File("C:/Path/to/file3.py"), "main3", 0, new HashSet<Import>());

        FunctionFactory.reset();
        Function main0 = FunctionFactory.create(fh1);
        Function main1 = FunctionFactory.create(fh2);
        Function main2 = FunctionFactory.create(fh3);
        Function main3 = FunctionFactory.create(fh4);

        main0.setParameter(main3, 0);
        main2.setParameter(main0, 0);
        main2.setParameter(main1, 1);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file0', 'C:\\Path\\to\\file0.py')"));
        expScripts.add(new LineOfCode("s2 = imp.load_source('file3', 'C:\\Path\\to\\file3.py')"));
        expScripts.add(new LineOfCode("s3 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        
        linesOfCode.add(new LineOfCode("v1 = s2.main3 (  )"));
        linesOfCode.add(new LineOfCode("v2 = copy.deepcopy ( v1 )"));
        linesOfCode.add(new LineOfCode("v0 = s1.main0 ( v2 )"));
        linesOfCode.add(new LineOfCode("v3 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("v4 = s3.main1 (  )"));
        linesOfCode.add(new LineOfCode("v5 = copy.deepcopy ( v4 )"));
        linesOfCode.add(new LineOfCode("s0.main2 ( v3, v5 )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(main2);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());

        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }
        
        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate7() {
        System.out.println("create7");

        /* GIVEN
         * main0 |
         *       |-> main1
         * main0 |
         */
        FunctionHeader fh0 = new FunctionHeader(new File("C:/Path/to/file0.py"), "main0", 0, new HashSet<Import>());
        FunctionHeader fh1 = new FunctionHeader(new File("C:/Path/to/file1.py"), "main1", 2, new HashSet<Import>());

        FunctionFactory.reset();
        Function main0 = FunctionFactory.create(fh0);
        Function main1 = FunctionFactory.create(fh0);
        Function main2 = FunctionFactory.create(fh1);

        main2.setParameter(main0, 0);
        main2.setParameter(main1, 1);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file0', 'C:\\Path\\to\\file0.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("v0 = s1.main0 (  )"));
        linesOfCode.add(new LineOfCode("v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("v2 = s1.main0 (  )"));
        linesOfCode.add(new LineOfCode("v3 = copy.deepcopy ( v2 )"));
        linesOfCode.add(new LineOfCode("s0.main1 ( v1, v3 )"));

        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.create(main2);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());

        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }
        
        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate8() {
        System.out.println("create8 (with gateway)");

        // GIVEN || main
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file.py"), "main", 0, imports);

        FunctionFactory.reset();
        Evaluable leaf = FunctionFactory.create(fh1);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> preCode = new LinkedList<>();
        preCode.add(new LineOfCode("gateway = JavaGateway()"));
        preCode.add(new LineOfCode("communicator = gateway.entry_point.getCommunicator()"));
        preCode.add(new LineOfCode("communicator.programStart()"));
        preCode.add(new LineOfCode("try:"));
        preCode.add(new LineOfCode("    try:"));
        preCode.add(new LineOfCode("        s0 = imp.load_source('file', 'C:\\Path\\to\\file.py')"));
        preCode.add(new LineOfCode("    except Exception as e:"));
        preCode.add(new LineOfCode("        communicator.onException( None , str(e) )"));
        preCode.add(new LineOfCode("        quit()"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));
        expImports.add(new Import("py4j.java_gateway", "JavaGateway"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        linesOfCode.add(new LineOfCode("    try:"));
        linesOfCode.add(new LineOfCode("        s0.main (  )"));
        linesOfCode.add(new LineOfCode("    except Exception as e:"));
        linesOfCode.add(new LineOfCode("        communicator.onException( 0 , str(e) )"));
        linesOfCode.add(new LineOfCode("        quit()"));
        linesOfCode.add(new LineOfCode("except Exception as e:"));
        linesOfCode.add(new LineOfCode("    communicator.onException( None , str(e) )"));
        linesOfCode.add(new LineOfCode("communicator.programEnd()"));

        Program expResult = new Program(expImports, preCode, linesOfCode);
        Program result = instance.create(true, leaf);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }

    @Test
    public void testCreate9() {
        System.out.println("create9 (with gateway)");

        // GIVEN || main
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));
        
        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file.py"), "main1", 0, imports);
        FunctionHeader fh2 = new FunctionHeader(new File("C:\\Path\\to\\file.py"), "main2", 1, imports);

        Set<FunctionHeader> functionHeaders = new HashSet<>();
        functionHeaders.add(fh1);
        functionHeaders.add(fh2);

        FunctionFactory.reset();
        Function f1 = FunctionFactory.create(fh1);
        Function f2 = FunctionFactory.create(fh2);

        f2.setParameter(f1, 0);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> preCode = new LinkedList<>();
        // pre
        preCode.add(new LineOfCode("gateway = JavaGateway()"));
        preCode.add(new LineOfCode("communicator = gateway.entry_point.getCommunicator()"));
        preCode.add(new LineOfCode("communicator.programStart()"));
        preCode.add(new LineOfCode("try:"));
        preCode.add(new LineOfCode("    try:"));
        preCode.add(new LineOfCode("        s0 = imp.load_source('file', 'C:\\Path\\to\\file.py')"));
        preCode.add(new LineOfCode("    except Exception as e:"));
        preCode.add(new LineOfCode("        communicator.onException( None , str(e) )"));
        preCode.add(new LineOfCode("        quit()"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));
        expImports.add(new Import("py4j.java_gateway", "JavaGateway"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        // code
        linesOfCode.add(new LineOfCode("    try:"));
        linesOfCode.add(new LineOfCode("        v0 = s0.main1 (  )"));
        linesOfCode.add(new LineOfCode("    except Exception as e:"));
        linesOfCode.add(new LineOfCode("        communicator.onException( 0 , str(e) )"));
        linesOfCode.add(new LineOfCode("        quit()"));
        linesOfCode.add(new LineOfCode("    v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("    try:"));
        linesOfCode.add(new LineOfCode("        s0.main2 ( v1 )"));
        linesOfCode.add(new LineOfCode("    except Exception as e:"));
        linesOfCode.add(new LineOfCode("        communicator.onException( 1 , str(e) )"));
        linesOfCode.add(new LineOfCode("        quit()"));

        // post
        linesOfCode.add(new LineOfCode("except Exception as e:"));
        linesOfCode.add(new LineOfCode("    communicator.onException( None , str(e) )"));
        linesOfCode.add(new LineOfCode("communicator.programEnd()"));

        Program expResult = new Program(expImports, preCode, linesOfCode);
        Program result = instance.create(true, f2);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }
    
    @Test
    public void testCreate10() {
        System.out.println("create10 (loop)");

        // GIVEN || loop (main1)
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file1.py"), "main1", 0, imports);

        FunctionFactory.reset();
        Function main1 = FunctionFactory.create(fh1);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        
        linesOfCode.add(new LineOfCode("v0 = s0.main1 (  )"));
        
        linesOfCode.add(new LineOfCode("while v0 is not None:"));
        linesOfCode.add(new LineOfCode("    v0 = s0.main1 (  )"));
        
        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.createLoop(false, main1);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }
    
    @Test
    public void testCreate11() {
        System.out.println("create11 (loop)");

        // GIVEN || loop (main1) -> main2
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file1.py"), "main1", 0, imports);
        FunctionHeader fh2 = new FunctionHeader(new File("C:\\Path\\to\\file2.py"), "main2", 1, new HashSet<Import>());

        FunctionFactory.reset();
        Function main1 = FunctionFactory.create(fh1);
        Function main2 = FunctionFactory.create(fh2);
        
        main2.setParameter(main1, 0);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        List<LineOfCode> expScripts = new LinkedList<>();
        expScripts.add(new LineOfCode("s0 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));
        expScripts.add(new LineOfCode("s1 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        
        linesOfCode.add(new LineOfCode("v0 = s1.main1 (  )"));
        
        linesOfCode.add(new LineOfCode("while v0 is not None:"));
        linesOfCode.add(new LineOfCode("    v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("    s0.main2 ( v1 )"));
        linesOfCode.add(new LineOfCode("    v0 = s1.main1 (  )"));
        
        Program expResult = new Program(expImports, expScripts, linesOfCode);
        Program result = instance.createLoop(false, main2);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            System.out.println("Expec: ");
            for(LineOfCode l: expResult.preCode()) {
                System.out.println(l.getCode());
            }
            
            System.out.println("was: ");
            for(LineOfCode l: result.preCode()) {
                System.out.println(l.getCode());
            }
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }
    
    @Test
    public void testCreate12() {
        System.out.println("create12 (loop & gateway)");

        // GIVEN || loop (main1) -> main2
        Set<Import> imports = new HashSet<>();
        imports.add(new Import("math"));

        FunctionHeader fh1 = new FunctionHeader(new File("C:\\Path\\to\\file1.py"), "main1", 0, imports);
        FunctionHeader fh2 = new FunctionHeader(new File("C:\\Path\\to\\file2.py"), "main2", 1, new HashSet<Import>());

        FunctionFactory.reset();
        Function main1 = FunctionFactory.create(fh1);
        Function main2 = FunctionFactory.create(fh2);
        
        main2.setParameter(main1, 0);

        // EXPECTED
        ProgramFactory instance = new ProgramFactory();
        
        // pre 
        List<LineOfCode> preCode = new LinkedList<>();
        
        preCode.add(new LineOfCode("gateway = JavaGateway()"));
        preCode.add(new LineOfCode("communicator = gateway.entry_point.getCommunicator()"));
        preCode.add(new LineOfCode("communicator.programStart()"));
        preCode.add(new LineOfCode("try:"));
        preCode.add(new LineOfCode("    try:"));
        preCode.add(new LineOfCode("        s0 = imp.load_source('file2', 'C:\\Path\\to\\file2.py')"));
        preCode.add(new LineOfCode("    except Exception as e:"));
        preCode.add(new LineOfCode("        communicator.onException( None , str(e) )"));
        preCode.add(new LineOfCode("        quit()"));
        preCode.add(new LineOfCode("    try:"));
        preCode.add(new LineOfCode("        s1 = imp.load_source('file1', 'C:\\Path\\to\\file1.py')"));
        preCode.add(new LineOfCode("    except Exception as e:"));
        preCode.add(new LineOfCode("        communicator.onException( None , str(e) )"));
        preCode.add(new LineOfCode("        quit()"));

        Set<Import> expImports = new HashSet<>();
        expImports.add(new Import("copy"));
        expImports.add(new Import("imp"));
        expImports.add(new Import("py4j.java_gateway", "JavaGateway"));

        List<LineOfCode> linesOfCode = new LinkedList<>();
        // code
        linesOfCode.add(new LineOfCode("    try:"));
        linesOfCode.add(new LineOfCode("        v0 = s1.main1 (  )"));
        linesOfCode.add(new LineOfCode("    except Exception as e:"));
        linesOfCode.add(new LineOfCode("        communicator.onException( 0 , str(e) )"));
        linesOfCode.add(new LineOfCode("        quit()"));
        
        linesOfCode.add(new LineOfCode("    while v0 is not None:"));
        linesOfCode.add(new LineOfCode("        v1 = copy.deepcopy ( v0 )"));
        linesOfCode.add(new LineOfCode("        try:"));
        linesOfCode.add(new LineOfCode("            s0.main2 ( v1 )"));
        linesOfCode.add(new LineOfCode("        except Exception as e:"));
        linesOfCode.add(new LineOfCode("            communicator.onException( 1 , str(e) )"));
        linesOfCode.add(new LineOfCode("            quit()"));
        linesOfCode.add(new LineOfCode("        try:"));
        linesOfCode.add(new LineOfCode("            v0 = s1.main1 (  )"));
        linesOfCode.add(new LineOfCode("        except Exception as e:"));
        linesOfCode.add(new LineOfCode("            communicator.onException( 0 , str(e) )"));
        linesOfCode.add(new LineOfCode("            quit()"));
        
        // post
        linesOfCode.add(new LineOfCode("except Exception as e:"));
        linesOfCode.add(new LineOfCode("    communicator.onException( None , str(e) )"));
        linesOfCode.add(new LineOfCode("communicator.programEnd()"));
        
        Program expResult = new Program(expImports, preCode, linesOfCode);
        Program result = instance.createLoop(true, main2);

        // TEST imports
        assertEquals(expResult.getImports(), result.getImports());
        
        // TEST script imports
        if (expResult.preCode().size() != result.preCode().size()) {
            System.out.println("Expec: ");
            for(LineOfCode l: expResult.preCode()) {
                System.out.println(l.getCode());
            }
            
            System.out.println("was: ");
            for(LineOfCode l: result.preCode()) {
                System.out.println(l.getCode());
            }
            fail("EXPECTED lenght " + expResult.preCode().size() + ", but was " + result.preCode().size());
        }

        for (int i = 0; i < result.preCode().size(); i++) {
            assertEquals(expResult.preCode().get(i).getCode(), result.preCode().get(i).getCode());
        }

        // TEST linesOfCode
        if (expResult.getLinesOfCode().size() != result.getLinesOfCode().size()) {
            fail("EXPECTED lenght " + expResult.getLinesOfCode().size() + ", but was " + result.getLinesOfCode().size());
        }

        for (int i = 0; i < result.getLinesOfCode().size(); i++) {
            for(LineOfCode l: result.getLinesOfCode()) {
                System.out.println(l.getCode());
            }
            assertEquals(expResult.getLinesOfCode().get(i).getCode(), result.getLinesOfCode().get(i).getCode());
        }
    }
}
